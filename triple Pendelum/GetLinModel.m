%% Linearisiertes Modell
% Unteraktuierter Roboter - Projektseminar Robotik
%

function [A,B,C,D] = GetLinModel(x_AP,u_AP,allParam)

g_val = allParam(1);
m_val = allParam(2);
m1_val = allParam(3);
m2_val = allParam(4);
m3_val = allParam(5);
l1_val = allParam(6);
l2_val = allParam(7);
l3_val = allParam(8);
Rs_val = allParam(9);
R1_val = allParam(10);
R2_val = allParam(11);
R3_val = allParam(12);

load('symSol.mat','ddxs_sol','ddphi1_sol','ddphi2_sol','ddphi3_sol',...
    'phi1','phi2','phi3',...
    'dphi1','dphi2','dphi3',...
    'ddphi1','ddphi2','ddphi3',...
    'xs','dxs','ddxs',...
    'm1','m2','m3','m',...
    'l1','l2','l3',...
    'F','g','t',...
    'R1','R2','R3','Rs')

%% Nichtlineares Modell:
% dx=f(x,u)
f = [ddxs_sol;
    ddphi1_sol;
    ddphi2_sol;
    ddphi3_sol];

%% Linearisierung:
% dx=Ax+Bu  y=Cx
% A, B, C, D bestimmen

% Zustände
x = [xs; dxs; phi1; dphi1; phi2; dphi2; phi3; dphi3;];
u = [F];
% Arbeitspunkt
%x_AP
%u_AP

% Linearisierung:
A = jacobian(f,x);
B = jacobian(f,u);
A = subs(A,[x;u],[x_AP;u_AP]);
B = subs(B,[x;u],[x_AP;u_AP]);
C = [1,0,0,0,0,0,0,0;
    0,0,1,0,0,0,0,0;
    0,0,0,0,1,0,0,0;
    0,0,0,0,0,0,1,0];
D = zeros(4,1);

% Parameter einsetzen:
A = double(subs(A,[g m m1 m2 m3 l1 l2 l3 Rs R1 R2 R3],[g_val m_val m1_val m2_val m3_val l1_val l2_val l3_val Rs_val R1_val R2_val R3_val]));
B = double(subs(B,[g m m1 m2 m3 l1 l2 l3 Rs R1 R2 R3],[g_val m_val m1_val m2_val m3_val l1_val l2_val l3_val Rs_val R1_val R2_val R3_val]));

% Alle Zustände:
A = [0 1 0 0 0 0 0 0;
    A(1,:);
    0 0 0 1 0 0 0 0;
    A(2,:);
    0 0 0 0 0 1 0 0;
    A(3,:);
    0 0 0 0 0 0 0 1;
    A(4,:)];
B = [0;
    B(1,:);
    0;
    B(2,:);
    0;
    B(3,:);
    0;
    B(4,:)];

end