%% Allgemeine Untersuchungen
% Unteraktuierter Roboter - Projektseminar Robotik
%

clear
clc

%% Linearisiertes Modell laden
% x_AP: xs dxs phi1 dphi1 phi2 dphi2 phi3 dphi3
x_AP = [0; 0; pi; 0; pi; 0; pi; 0];
% u_AP: F
u_AP = [0];
initModell
[A,B,C,D] = GetLinModel(x_AP,u_AP,allParam);
sys = ss(A,B,C,D)

%% Systemeigenschaften:
%Zustände n:
n = size(A,1);
%Eingänge p:
p = size(B,2);
%Ausgänge q:
q = size(C,1);
disp(['Zustände: ' num2str(n) ', Eingänge: ' num2str(p) ', Ausgänge: ' num2str(q)])
if q == p
    disp('Das System ist quadratisch.')
else
    disp('Das System ist nicht quadratisch.')
end

% Übertragungsmatrix G:
disp('Übertragungsmatrix G:')
s=tf('s');
G = C*inv(s*eye(n)-A)*B

% Differenzenordnung gamma(i) für jeden Ausgang, i=1,..,q:
gamma = ones(1,q);
for i=1:q %Schleife über Zeilen von C
    while all(C(i, :)*A^(gamma(i) - 1)*B == zeros(1, q))
        gamma(i) = gamma(i) + 1;
    end
end
disp(['Differenzordnungen: ' num2str(gamma)])
gamma_total = sum(gamma);
disp(['Gesamtdifferenzordnung: ' num2str(gamma_total)])

% Eigenwerte EW, Eigenvektoren WV, Pole P:
disp('Eigenwerte EW, Eigenvektoren WV, Pole P:');
[EV,EW] = eig(A,'vector')
P = pole(sys)

% Invariante Nullstellen N, Übertragungsnullstellen UEN:
disp('Invariante Nullstellen N, Übertragungsnullstellen UEN:')
N = tzero(sys)
UEN = zeros(0); %Übertragungsnullstellen:
for i = 1:length(N)
    if rank(evalfr(G,N(i))) < min(size(G)) % rank(G(N(i)) < min(rank(G)) !! nicht ganz richtig, weis nicht wie ich min(rank(G)) mache
    disp(['Übertragungsnullstelle: ' num2str(N(i) )])
    UEN(end+1,1) = N(i);
    end
end
UEN

% Invariante Nullstellen selbst berechnen: !! Existieren nur bei gamma_total < n
% if gamma_total < n
%     H_temp = [ A,  B;
%          -C, -D];
%     E_temp = [eye(n, n),   zeros(n, p);
%          zeros(p, n), zeros(p, p)];
%     N_own = eig(H_temp, E_temp); % Übungsskript Mehrgrößenregler S. 40
%     N_own(isinf(N_own)) = [] % nach S.44 Nullstellen mit unendlichem Betrag ignorieren
% else
%     disp('System hat keine invarianten Nullstellen')
% end

% Stabile Entkoppelbarkeit prüfen:
if any(real(N) >= 0)
    disp('System ist nicht stabil entkoppelbar');
else
    disp('System ist stabil entkoppelbar');
end

%% Steuerbarkeit:

% Kalman:
str = ctrb(A,B); % Steuerbarkeitsmatrix
if rank(str) == length(A) % Bei quadr. System det(str) ~= 0
    disp('System ist vollständig steuerbar - (Kalman)')
else
    disp('System ist nicht steuerbar - (Kalman)')
end

% Hautus:
vollststr = 1;
EWS = zeros(0);
EWNS = zeros(0);
for i = 1:n
    M=[EV(i)*eye(n)-A, B];
    if rank(M)==n
        EWS(end+1,1) = EV(i);
    else
        vollststr = 0;
        EWNS(end+1,1) = EV(i);
    end
end
EWS
EWNS
if vollststr == 1
    disp('System ist vollständig steuerbar - (Hautus)')
else
    disp('System ist nicht steuerbar - (Hautus)')
end

%% Beobachtbarkeit:

% Kalman:
beob = obsv(A,C); % Beobachtbarkeitsmatrix
if rank(beob) == length(A) % Bei quadr. System det(beob) ~= 0
    disp('System ist vollständig beobachtbar - (Kalman)')
else
    disp('System ist nicht beobachtbar - (Kalman)')
end

% Hautus:
vollstbeob = 1;
EWB = zeros(0);
EWNB = zeros(0);
for i=1:n
    M=[EV(i)*eye(n)-A; C];
    if rank(M)==n
        EWB(end+1,1) = EV(i);
    else
        vollstbeob = 0;
        EWNB(end+1,1) = EV(i);
    end
end
EWB
EWNB
if vollstbeob == 1
    disp('System ist vollständig beobachtbar - (Hautus)')
else
    disp('System ist nicht beobachtbar - (Hautus)')
end


