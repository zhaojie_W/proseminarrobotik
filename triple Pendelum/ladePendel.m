function stPendel = ladePendel()
    stPendel.g = 9.81;
    stPendel.m = 0.3;
    stPendel.m1 = 0.3;
    stPendel.m2 = 0.3;
    stPendel.m3 = 0.3;
    stPendel.l1 = 0.2;
    stPendel.l2 = 0.2;
    stPendel.l3 = 0.2;
    stPendel.Rs = 0;
    stPendel.R1 = 0;
    stPendel.R2 = 0;
    stPendel.R3 = 0;
    
end 