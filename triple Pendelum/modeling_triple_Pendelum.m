%% Modellierung
% Unteraktuierter Roboter - Projektseminar Robotik
%

clear
clc

%% Symbolische Variablen:
% Zustände
syms phi1 phi2 phi3
syms dphi1 dphi2 dphi3
syms ddphi1 ddphi2 ddphi3
syms xs dxs ddxs
% Parameter und Kräfte/Momente
syms m1 m2 m3 m
syms l1 l2 l3
syms F g t
syms Rs R1 R2 R3

%% Position und Geschwindigkeit:
% s1
s_1x = xs + 1/2 * l1 * sin(phi1);
s_1y = -1/2 * l1 * cos(phi1);
% ds1
ds_1x = dxs + 1/2 * l1 * cos(phi1) * dphi1;
ds_1y = 1/2 * l1 * sin(phi1) * dphi1;

% s2
s_2x = xs + l1 * sin(phi1) + 1/2 * l2 * sin(phi2);
s_2y = -l1 * cos(phi1) - 1/2 * l2 * cos(phi2) ;
% ds2
ds_2x = dxs + l1 * cos(phi1) * dphi1 + 1/2 * l2 * cos(phi2) * dphi2;
ds_2y = l1 * sin(phi1) * dphi1 + 1/2 * l2 * sin(phi2) * dphi2;

% s3
s_3x = xs + l1 * sin(phi1) + l2 * sin(phi2) + 1/2 * l3 * sin(phi3);
s_3y = -l1 * cos(phi1) - l2 * cos(phi2) - 1/2 * l3 * cos(phi3);
% ds3
ds_3x = dxs + l1 * cos(phi1) * dphi1 + l2 * cos(phi2) * dphi2 + 1/2 * l3 * cos(phi3) * dphi3;
ds_3y = l1 * sin(phi1) * dphi1 + l2 * sin(phi2) * dphi2 + 1/2 * l3 * sin(phi3) * dphi3;


%% Lagrange:
% T:
T_K = 1/2 * m * dxs^2;
T_1 = 1/2 * m1 * (ds_1x^2 + ds_1y^2) + 1/2 * (1/12 * m1 * l1^2) * dphi1^2;
T_2 = 1/2 * m2 * (ds_2x^2 + ds_2y^2) + 1/2 * (1/12 * m2 * l2^2) * dphi2^2;
T_3 = 1/2 * m3 * (ds_3x^2 + ds_3y^2) + 1/2 * (1/12 * m3 * l3^2) * dphi3^2;



% U:
U_K = 0;
U_1 = m1 * g * s_1y;
U_2 = m2 * g * s_2y;
U_3 = m3 * g * s_3y;

% Rayleigh dissipation function
R = 1/2 * R1 * dphi1^2 + 1/2 * R2 * (dphi2 - dphi1)^2 + 1/2 * R3 * (dphi3 - dphi2)^2;

% dR/dphi1
R_dphi1 = jacobian(R,dphi1);
% dR/dphi2
R_dphi2 = jacobian(R,dphi2);
% dR/dphi3
R_dphi3 = jacobian(R,dphi3);

% L:
L = T_K + T_1 + T_2 + T_3 - U_K - U_1 - U_2 - U_3;
% L = T_K + T_1 + T_2 - U_K -U_1 - U_2;
% Herleitung der Ableitung nach generalisierten Koordinaten:
% dL/xs
L_xs = jacobian(L,xs);
% dL/phi1
L_phi1 = jacobian(L,phi1);
% dL/phi2
L_phi2 = jacobian(L,phi2);
% dL/phi3
L_phi3 = jacobian(L,phi3);


% dL/dxs
L_dxs = jacobian(L,dxs);
% dL/dphi1
L_dphi1 = jacobian(L,dphi1);
% dL/dphi2
L_dphi2 = jacobian(L,dphi2);
% dL/dphi3
L_dphi3 = jacobian(L,dphi3);


% Variablen ohne t durch Variablen mit t ersetzen:
L_dxs_t = subs(L_dxs,{xs,phi1,phi2,phi3,dxs,dphi1,dphi2,dphi3}, ...
    str2sym({'xs(t)','phi1(t)','phi2(t)','phi3(t)','dxs(t)','dphi1(t)','dphi2(t)','dphi3(t)'}));
L_dphi1_t = subs(L_dphi1,{xs,phi1,phi2,phi3,dxs,dphi1,dphi2,dphi3}, ...
    str2sym({'xs(t)','phi1(t)','phi2(t)','phi3(t)','dxs(t)','dphi1(t)','dphi2(t)','dphi3(t)'}));
L_dphi2_t = subs(L_dphi2,{xs,phi1,phi2,phi3,dxs,dphi1,dphi2,dphi3}, ...
    str2sym({'xs(t)','phi1(t)','phi2(t)','phi3(t)','dxs(t)','dphi1(t)','dphi2(t)','dphi3(t)'}));
L_dphi3_t = subs(L_dphi3,{xs,phi1,phi2,phi3,dxs,dphi1,dphi2,dphi3}, ...
    str2sym({'xs(t)','phi1(t)','phi2(t)','phi3(t)','dxs(t)','dphi1(t)','dphi2(t)','dphi3(t)'}));

% Berechnung der Zeitableitungen:
dL_dxs_t = diff(L_dxs_t,t);
dL_dphi1_t = diff(L_dphi1_t,t);
dL_dphi2_t = diff(L_dphi2_t,t);
dL_dphi3_t = diff(L_dphi3_t,t);

% Variablen mit t
Var_t = str2sym({'xs(t)','dxs(t)','diff(xs(t),t)','diff(dxs(t),t)',...
    'phi1(t)','dphi1(t)','diff(phi1(t),t)','diff(dphi1(t),t)',...
    'phi2(t)','dphi2(t)','diff(phi2(t),t)','diff(dphi2(t),t)',...
    'phi3(t)','dphi3(t)','diff(phi3(t),t)','diff(dphi3(t),t)'});
% Variablen ohne t
Var_ot = {xs,dxs,dxs,ddxs,...
          phi1,dphi1,dphi1,ddphi1,...
          phi2,dphi2,dphi2,ddphi2,...
          phi3,dphi3,dphi3,ddphi3};

% Variablen mit t durch Variablen ohne t wieder ersetzen:
dL_dxs_t = subs(dL_dxs_t,Var_t,Var_ot);
dL_dphi1_t = subs(dL_dphi1_t,Var_t,Var_ot);
dL_dphi2_t = subs(dL_dphi2_t,Var_t,Var_ot);
dL_dphi3_t = subs(dL_dphi3_t,Var_t,Var_ot);


% Berechnung der LAGRANGEschen Gleichungen:   
Sol = solve([dL_dxs_t - L_xs  == F - Rs * dxs,...
            dL_dphi1_t - L_phi1 + R_dphi1== 0,...
            dL_dphi2_t - L_phi2 + R_dphi2 == 0,...
            dL_dphi3_t - L_phi3 + R_dphi3 == 0],...
            [ddxs,ddphi1,ddphi2,ddphi3]);
        

ddxs_sol = simplify(Sol.ddxs);
ddphi1_sol = simplify(Sol.ddphi1);
ddphi2_sol = simplify(Sol.ddphi2);
ddphi3_sol = simplify(Sol.ddphi3);


%% Speichern der sym Gleichungen:

save('symSol.mat','ddxs_sol','ddphi1_sol','ddphi2_sol','ddphi3_sol',...
    'phi1','phi2','phi3',...
    'dphi1','dphi2','dphi3',...
    'ddphi1','ddphi2','ddphi3',...
    'xs','dxs','ddxs',...
    'm1','m2','m3','m',...
    'l1','l2','l3',...
    'F','g','t',...
    'R1','R2','R3','Rs')

