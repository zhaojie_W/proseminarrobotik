function animierePendel(vT, mX, stPendel, hAxes, film)
    % Anzahl von Input bestimmen, ob Film machen.
    switch nargin
        case 4
            obfilm = 0;
        case 5
            obfilm = 1;
    end
    
    % Parameter laden
    l1 = stPendel.l1;
    l2 = stPendel.l2;
    l3 = stPendel.l3;
    % Parametern von Animation bestimmen. -Pausezeit jedes Bild, Anzahl von Bildern. 
    Pause = 0.025;
    vTAnim = 0:Pause:vT(end);
    nBilder = length(vTAnim);
    % Figur/Axes initialisieren.
    handleF1 = figure();
    handleAxes1 = axes();
    if isempty(hAxes)
        handleF2 = figure();
        handleAxes2 = axes();        
        axis([-1, 1, -0.7, 0.7]);  
    else 
        axis(hAxes);
    end
    hold on ;
    % Umlaufbahn plot
    bahn = plot([-1,1],[0 0],'k');
    % Cart und Pendels plot, und Film machen wenn film == 1.
    for i = 1:nBilder
        mX_i = interp1(vT,mX,vTAnim(i));
        xs = mX_i(1);
        phi1 = mX_i(3);
        phi2 = mX_i(5);
        phi3 = mX_i(7);
        x1 = xs + l1 * sin(phi1);
        x2 = l2 * sin(phi2) + x1;
        x3 = l3 * sin(phi3) + x2;
        y1 = -l1 * cos(phi1);
        y2 = -l2 * cos(phi2) + y1;
        y3 = -l3 * cos(phi3) + y2;
        inX1 = [xs,x1];
        inX2 = [x1,x2];
        inX3 = [x2,x3];
        inY1 = [0,y1];
        inY2 = [y1,y2];
        inY3 = [y2,y3];
        if i>1
            delete(cart);
            delete(hPunkt);
            delete(title1);
        end
        pos_cart = [xs-0.05 -0.025 0.1 0.05];
        cart = rectangle('position',pos_cart,'FaceColor','k'); 
        hPunkt = plot(inX1,inY1,'b',inX2,inY2,'r', inX3,inY3,'k');
        title1 = title(['t = ', num2str(vTAnim(i)),'sec']);
        pause(Pause);
        if obfilm ==1
            if film ==1
                vFrames(i) = getframe(gcf);
            end
        end
        
    end
    hold off;
    
    % Film speichern
    if obfilm ==1
        if film ==1
            v = VideoWriter('animierePendel.avi');
            open(v);
            writeVideo(v,vFrames);
            close(v);
            
        end
    end

end 
